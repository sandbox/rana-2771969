CONTENTS OF THIS FILE
---------------------

 * INTRODUCTION
 * INSTALLATION
 * MAINTAINERS

INTRODUCTION
------------
This module will help to restrict disposable email id's (Temporary email id's)
to be submitted in your webform.
In this module you just have to add the Webform id's and that webform will be 
protected for disposable email id's and if you want to add more you can add at
your own form the configuration.

INSTALLATION
------------
1. Check requirements section first.
2. Enable the module.
https://www.drupal.org/documentation/install/modules-themes/modules-7

MAINTAINERS
-----------
Current maintainers:
 * Tushar Rana(tushar-rana) - https://www.drupal.org/user/3278169
 * Neha Pandya(nehapandya55) - https://www.drupal.org/user/2848621
